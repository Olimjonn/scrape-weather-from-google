from django.shortcuts import render
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import  Weather
from .scraper import scrape_weather_data
from .serializers import *
from django.http import JsonResponse


class WeatherView(APIView):
    def get(self, request, *args, **kwargs):
        city_name = request.GET.get("city")
        weather_data = scrape_weather_data(city_name)
        temperature = weather_data.get('temperature')
        description = weather_data.get('description')

        Weather.objects.create(
            city=city_name,
            temperature=temperature,
            description=description
        )
        return JsonResponse({'temperature': temperature, 'description': description})

