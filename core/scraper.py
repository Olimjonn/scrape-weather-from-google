import requests
from bs4 import BeautifulSoup


def scrape_weather_data(city_name):
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36"
    }
    url = f"https://www.google.com/search?q=weather+{city_name}"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    temperature = soup.find('div', class_='BNeawe iBp4i AP7Wnd').text
    description = soup.find('div', class_='BNeawe tAd8D AP7Wnd').text
    return {'temperature': temperature, 'description': description}
