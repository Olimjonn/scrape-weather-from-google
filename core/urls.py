from django.urls import path
from core.views import WeatherView

urlpatterns = [
       path('weather/', WeatherView.as_view(), name='weather'),
]