from django.db import models


# class City(models.Model):
#        name = models.CharField(max_length=100)

#        def __str__(self):
#            return self.name

class Weather(models.Model):
       city = models.CharField(max_length=100)
       temperature = models.CharField(max_length=10)
       description = models.CharField(max_length=100)

       def __str__(self):
           return f"{self.city}: {self.temperature} ({self.description})"
